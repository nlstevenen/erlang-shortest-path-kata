%%%-------------------------------------------------------------------
%%% @author joudejans, skok
%%% @copyright (C) 2014, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 20. Oct 2014 5:28 PM
%%%-------------------------------------------------------------------
-module(routeCalculator).
-author("joudejans").
-author("skok").

%% API
-export([calculateRoute/3]).
-import(erlang, [display/1, format/2]).
-import(pathOperations, [getPathToEndNode/2, getShortestPath/1, getNeighbours/2, addShortestPath/3, updateShortestPathForAllNeighbours/5, doesNodeExistInListAsLastElement/2]).

isShortestPathFound(LastNodeFromCurrentPath, EndNode) ->
  if (LastNodeFromCurrentPath == EndNode) -> true;
    true -> false
  end.

%% V = all nodes
%% X = all nodes that are found so far via neighbour search
%% A = all finished nodes with shortest path from start to this node
calculateRoute(StartNode, EndNode, Edges) ->
% Add start node to X
  FinishedPaths = calculateRoute(EndNode, Edges, [{[StartNode], 0}], []),
% After all paths are found, get the shortest path from start to finish
  getPathToEndNode(EndNode, FinishedPaths).

calculateRoute(_, _, [], FinishedPaths) ->
  FinishedPaths;
calculateRoute(EndNode, Paths, ShortestPaths, FinishedPaths) ->
  % Get shortest path from X
  {PathCurrentNode, DistanceCurrentNode} = getShortestPath(ShortestPaths),
  % Last node from shortest path
  LastNodeFromCurrentNodeRoute = lists:last(PathCurrentNode),

  IsShortestPathFound = isShortestPathFound(LastNodeFromCurrentNodeRoute, EndNode),

  if (IsShortestPathFound) -> FinishedPaths;
    true -> true
  end,

  ShortestPathsWithSmallestRemoved = lists:delete({PathCurrentNode, DistanceCurrentNode}, ShortestPaths),
  FinishedPathsWithNextNode = lists:append([{PathCurrentNode, DistanceCurrentNode}], FinishedPaths),
  Neighbours = getNeighbours(LastNodeFromCurrentNodeRoute, Paths),

  ToUpdate = lists:filter(fun(Neighbour) ->
    {N, _} = Neighbour,
    DoesNodeExistInListAsLastElement = doesNodeExistInListAsLastElement(N, ShortestPaths),
    if (DoesNodeExistInListAsLastElement) -> true;
      true -> false end
  end, Neighbours),

  ToAdd = lists:filter(fun(Neighbour) ->
    {N, _} = Neighbour,
    DoesExistInFinishedPaths = doesNodeExistInListAsLastElement(N, FinishedPaths),
    DoesExistInShortestPaths = doesNodeExistInListAsLastElement(N, ShortestPaths),

    if (DoesExistInFinishedPaths or DoesExistInShortestPaths) ->
      false;
      true -> true
    end
  end, Neighbours),

  LengthOfToAdd = length(ToAdd),
  if (LengthOfToAdd > 0) ->
    AddedNodes = addShortestPath({PathCurrentNode, DistanceCurrentNode}, ToAdd, ShortestPathsWithSmallestRemoved);
    true -> AddedNodes = ShortestPathsWithSmallestRemoved
  end,

  UpdatedNodes = updateShortestPathForAllNeighbours(ToUpdate, PathCurrentNode, DistanceCurrentNode, AddedNodes, []),

  FinalShortestPath = UpdatedNodes,

  SomethingToUpdate = length(FinalShortestPath) >= 1,
  if (SomethingToUpdate) ->
    calculateRoute(EndNode, Paths, FinalShortestPath, FinishedPathsWithNextNode);
    true -> calculateRoute(EndNode, Paths, AddedNodes, FinishedPathsWithNextNode)
  end.
