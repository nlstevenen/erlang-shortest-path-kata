%%%-------------------------------------------------------------------
%%% @author skok
%%% @copyright (C) 2014, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 29. okt 2014 09:54
%%%-------------------------------------------------------------------
-module(pathOperations).
-author("skok").

-export([getNeighbours/2, getDistanceBetweenNeighbours/3, getNearestNode/3, updateShortestPath/5, getShortestPath/1, getPathToEndNode/2, addShortestPath/3, updateShortestPathForAllNeighbours/5, doesNodeExistInListAsLastElement/2]).

getNeighbours(Node, Paths) ->
  getNeighbours(Node, Paths, []).

getNeighbours(Node, [{From, To, Distance} | RemainingNodes], Result) when Node == From ->
  getNeighbours(Node, RemainingNodes, lists:append(Result, [{To, Distance}]));
getNeighbours(Node, [{From, To, Distance} | RemainingNodes], Result) when Node == To ->
  getNeighbours(Node, RemainingNodes, lists:append(Result, [{From, Distance}]));
getNeighbours(Nodes, [_ | RemainingNodes], Result) ->
  getNeighbours(Nodes, RemainingNodes, Result);
getNeighbours(_, [], Result) ->
  Result.

getDistanceBetweenNeighbours(SearchFrom, SearchTo, [{NodeFrom, NodeTo, Distance} | _]) when SearchFrom == NodeFrom, SearchTo == NodeTo ->
  Distance;
getDistanceBetweenNeighbours(SearchFrom, SearchTo, [{NodeFrom, NodeTo, Distance} | _]) when SearchFrom == NodeTo, SearchTo == NodeFrom ->
  Distance;
getDistanceBetweenNeighbours(SearchFrom, SearchTo, [_ | RemainingPaths]) ->
  getDistanceBetweenNeighbours(SearchFrom, SearchTo, RemainingPaths);
getDistanceBetweenNeighbours(_, _, []) ->
  error(badArgument).

getNearestNode(From, NeighbourNodes, Paths) ->
  NeighbourDistances = lists:map(getListDistancesOfAllNeighboursFunction(From, Paths), NeighbourNodes),
  getSmallestNode(NeighbourDistances).

getListDistancesOfAllNeighboursFunction(From, Paths) ->
  fun(Neighbour) ->
    {Node, _} = Neighbour,
    {Neighbour, getDistanceBetweenNeighbours(From, Node, Paths)}
  end.

getSmallestNode([FirstTuple | RemainingTuples]) -> getSmallestNode(RemainingTuples, FirstTuple).

getSmallestNode([{_, D} | RemainingTuples], {Node, Distance}) when Distance < D ->
  getSmallestNode(RemainingTuples, {Node, Distance});
getSmallestNode([{N, D} | RemainingTuples], _) ->
  getSmallestNode(RemainingTuples, {N, D});
getSmallestNode([], {Node, Distance}) ->
  {Node, Distance}.

addShortestPath(From, Neighbours, ShortestPaths) ->
  lists:append(ShortestPaths, addShortestPath(From, Neighbours)).
addShortestPath(_, []) ->
  [];
addShortestPath({From, OriginalDistance}, [{Node, Distance} | RemainingNeighbours]) ->
  NewPath = lists:append(From, [Node]),
  [{NewPath, Distance + OriginalDistance} | addShortestPath({From, OriginalDistance}, RemainingNeighbours)].

updateShortestPathForAllNeighbours([], _, _, _, UpdatedPaths) ->
  UpdatedPaths;
updateShortestPathForAllNeighbours([{NewNode, Distance} | Remaining], PathToNewNode, DistanceOfPathToNewNode, ShortestPaths, _) ->
  UpdateShortestPath = updateShortestPath({NewNode, Distance}, PathToNewNode, DistanceOfPathToNewNode, ShortestPaths, []),
  updateShortestPathForAllNeighbours(Remaining, PathToNewNode, DistanceOfPathToNewNode, UpdateShortestPath, UpdateShortestPath).

updateShortestPath(_, _, _, [], UpdatedPaths) ->
  UpdatedPaths;
updateShortestPath({NewNode, Distance}, PathToNewNode, DistanceOfPathToNewNode, [{FirstShortestPath, CurrentDistance} | ShortestPaths], UpdatedPaths) ->
  LastElement = lists:last(FirstShortestPath),
  NewDistance = DistanceOfPathToNewNode + Distance,

  if ((LastElement == NewNode) and (CurrentDistance >= (NewDistance))) ->
    NewPath = lists:append(PathToNewNode, [NewNode]),
    updateShortestPath({NewNode, Distance}, PathToNewNode, DistanceOfPathToNewNode, ShortestPaths, [{NewPath, NewDistance} | UpdatedPaths]);
    true ->
      updateShortestPath({NewNode, Distance}, PathToNewNode, DistanceOfPathToNewNode, ShortestPaths, [{FirstShortestPath, CurrentDistance} | UpdatedPaths])
  end.

getShortestPath([{List, Distance} | []]) ->
  {List, Distance};
getShortestPath([{List, Distance} | Rest]) ->
  {ShortestList, ShortestDistance} = getShortestPath(Rest),
  if
    (Distance < ShortestDistance) -> {List, Distance};
    true -> {ShortestList, ShortestDistance}
  end.

getPathToEndNode(_, []) ->
  error(noRoutePossible);
getPathToEndNode(EndNode, [{Paths, Distance} | RemainingPaths]) ->
  LastNodeOfPath = lists:last(Paths),
  if
    (EndNode == LastNodeOfPath) -> {Paths, Distance};
    true -> getPathToEndNode(EndNode, RemainingPaths)
  end
.

doesNodeExistInListAsLastElement(Node, List) ->
  lists:any(fun(Element) ->
    {ElementList, _} = Element,
    LastElement = lists:last(ElementList),
    if (LastElement == Node) -> true;
      true -> false
    end
  end, List).