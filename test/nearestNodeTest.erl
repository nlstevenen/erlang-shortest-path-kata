%%%-------------------------------------------------------------------
%%% @author skok
%%% @copyright (C) 2014, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 22. okt 2014 10:07
%%%-------------------------------------------------------------------
-module(nearestNodeTest).
-author("skok").

-include_lib("eunit/include/eunit.hrl").
-import(pathOperations, [getNearestNode/3, getNeighbours/2]).
-import(testRouteBuilder, [square_createPaths/1, square_createNodes/0]).

nearestRouteOfAisB_test() ->
  Nodes = square_createNodes(),
  Paths = square_createPaths(Nodes),
  FirstNode = lists:nth(1, Nodes),
  Neighbours = getNeighbours(FirstNode, Paths),
  {{NearestNode, _}, _} = getNearestNode(FirstNode, Neighbours, Paths),
  ?assertEqual(lists:nth(2, Nodes), NearestNode).

nearestRouteOfAisBReversed_test() ->
  Nodes = square_createNodes(),
  Paths = lists:reverse(square_createPaths(Nodes)),
  FirstNode = lists:nth(1, Nodes),
  Neighbours = getNeighbours(FirstNode, Paths),
  {{NearestNode, _}, _} = getNearestNode(FirstNode, Neighbours, Paths),
  ?assertEqual(lists:nth(2, Nodes), NearestNode).

nearestRouteOfDisC_test() ->
  Nodes = square_createNodes(),
  Paths = square_createPaths(Nodes),
  FirstNode = lists:nth(4, Nodes),
  Neighbours = getNeighbours(FirstNode, Paths),
  {{NearestNode, _}, _} = getNearestNode(FirstNode, Neighbours, Paths),
  ?assertEqual(lists:nth(3, Nodes), NearestNode).

nearestRouteOfDisCReversed_test() ->
  Nodes = square_createNodes(),
  Paths = lists:reverse(square_createPaths(Nodes)),
  FirstNode = lists:nth(4, Nodes),
  Neighbours = getNeighbours(FirstNode, Paths),
  {{NearestNode, _}, _} = getNearestNode(FirstNode, Neighbours, Paths),
  ?assertEqual(lists:nth(3, Nodes), NearestNode).
