%%%-------------------------------------------------------------------
%%% @author stevensteven
%%% @copyright (C) 2014, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 25. Oct 2014 10:00
%%%-------------------------------------------------------------------
-module(getPathToEndNodeTest).
-author("stevensteven").

-include_lib("eunit/include/eunit.hrl").
-import(pathOperations, [getPathToEndNode/2]).

emptyPathListShouldThrowError_test() ->
  ?assertError(noRoutePossible, getPathToEndNode("a", [])).

nodeNotFoundShouldThrowError_test() ->
  ?assertError(noRoutePossible,getPathToEndNode("a", [{["b", "r"], 1}])).

shouldReturnLastNodeWithDistanceThree_test() ->
  PathToEndNode = getPathToEndNode("b", [{["a", "b"], 3}]),
  ?assertEqual(PathToEndNode, {["a", "b"], 3}).

shouldReturnLastNodeWithDistanceFour_test() ->
  PathToEndNode = getPathToEndNode("d", [{["a", "b"], 3}, {["a", "d"], 4}]),
  ?assertEqual(PathToEndNode, {["a", "d"], 4}).