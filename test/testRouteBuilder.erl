%%%-------------------------------------------------------------------
%%% @author skok
%%% @copyright (C) 2014, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 22. okt 2014 09:31
%%%-------------------------------------------------------------------
-module(testRouteBuilder).
-author("skok").

-export([square_createNodes/0, square_createPaths/1, ode_createPaths/1, ode_createNodes/0, invalid_createNodes/0, invalid_createPaths/1]).

nth(N, Nodes) ->
  lists:nth(N + 1, Nodes).

square_createNodes() ->
  ["a", "b", "c", "d"].
square_createPaths(Nodes) ->
  [{lists:nth(1, Nodes), lists:nth(2, Nodes), 1},
    {lists:nth(2, Nodes), lists:nth(3, Nodes), 1},
    {lists:nth(3, Nodes), lists:nth(4, Nodes), 1},
    {lists:nth(4, Nodes), lists:nth(1, Nodes), 4}].

ode_createNodes() ->
  ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13"].
ode_createPaths(Nodes) ->
  [{nth(0, Nodes), nth(1, Nodes), 15},
    {nth(0, Nodes), nth(2, Nodes), 45},
    {nth(0, Nodes), nth(3, Nodes), 22},
    {nth(1, Nodes), nth(7, Nodes), 30},
    {nth(2, Nodes), nth(7, Nodes), 10},
    {nth(3, Nodes), nth(7, Nodes), 60},
    {nth(7, Nodes), nth(8, Nodes), 8},
    {nth(7, Nodes), nth(11, Nodes), 25},
    {nth(7, Nodes), nth(12, Nodes), 36},
    {nth(8, Nodes), nth(9, Nodes), 40},
    {nth(8, Nodes), nth(11, Nodes), 5},
    {nth(9, Nodes), nth(10, Nodes), 50},
    {nth(10, Nodes), nth(13, Nodes), 55},
    {nth(11, Nodes), nth(12, Nodes), 5},
    {nth(11, Nodes), nth(13, Nodes), 12},
    {nth(12, Nodes), nth(13, Nodes), 20}].

invalid_createNodes() ->
  ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13"].
invalid_createPaths(Nodes) ->
  [{nth(0, Nodes), nth(1, Nodes), 15},
    {nth(0, Nodes), nth(2, Nodes), 45},
    {nth(11, Nodes), nth(12, Nodes), 5},
    {nth(11, Nodes), nth(13, Nodes), 12},
    {nth(12, Nodes), nth(13, Nodes), 20}].