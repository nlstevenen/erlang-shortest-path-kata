%%%-------------------------------------------------------------------
%%% @author joudejans
%%% @copyright (C) 2014, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 20. Oct 2014 5:28 PM
%%%-------------------------------------------------------------------
-module(getNeighboursTest).
-author("joudejans").

-include_lib("eunit/include/eunit.hrl").
-import(pathOperations, [getNeighbours/2]).
-import(testRouteBuilder, [square_createPaths/1, square_createNodes/0]).

getNeighboursOfFirstNode_test() ->
  Nodes = square_createNodes(),
  Paths = square_createPaths(Nodes),
  ?assertEqual([{"b", 1}, {"d", 4}], getNeighbours(lists:nth(1, Nodes), Paths)).

getNeighboursOfSecondNode_test() ->
  Nodes = square_createNodes(),
  Paths = square_createPaths(Nodes),
  ?assertEqual([{"a", 1}, {"c", 1}], getNeighbours(lists:nth(2, Nodes), Paths)).

getNeighboursOfThirdNode_test() ->
  Nodes = square_createNodes(),
  Paths = square_createPaths(Nodes),
  ?assertEqual([{"b", 1}, {"d", 1}], getNeighbours(lists:nth(3, Nodes), Paths)).
