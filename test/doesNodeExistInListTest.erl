%%%-------------------------------------------------------------------
%%% @author skok
%%% @copyright (C) 2014, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 27. okt 2014 09:38
%%%-------------------------------------------------------------------
-module(doesNodeExistInListTest).
-author("skok").

-include_lib("eunit/include/eunit.hrl").

-import(pathOperations, [doesNodeExistInListAsLastElement/2]).

shouldExistInList_test() ->
  DoesNodeExistInList = doesNodeExistInListAsLastElement("a", [{["a"], 0}]),
  ?assertEqual(true, DoesNodeExistInList).

shouldNotExistInLongList_test() ->
  DoesNodeExistInList = doesNodeExistInListAsLastElement("a", [{["a", "b"], 0}]),
  ?assertEqual(false, DoesNodeExistInList).

cShouldNotExistInLongList_test() ->
  DoesNodeExistInList = doesNodeExistInListAsLastElement("c", [{["a", "b"], 0}, {["b", "c", "d"], 5}]),
  ?assertEqual(false, DoesNodeExistInList).

cShouldExistInLongList_test() ->
  DoesNodeExistInList = doesNodeExistInListAsLastElement("c", [{["a", "b"], 0}, {["b", "c", "d"], 5}, {["a", "c"], 9}]),
  ?assertEqual(true, DoesNodeExistInList).

shouldNotExistInList_test() ->
  DoesNodeExistInList = doesNodeExistInListAsLastElement("b", [{["a"], 0}]),
  ?assertEqual(false, DoesNodeExistInList).
