%%%-------------------------------------------------------------------
%%% @author skok
%%% @copyright (C) 2014, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 24. okt 2014 16:46
%%%-------------------------------------------------------------------
-module(getShortestPathTest).
-author("skok").

-include_lib("eunit/include/eunit.hrl").
-import(pathOperations, [getShortestPath/1]).

firstTupleShouldBeShortest_test() ->
  ShortestPath = getShortestPath([{["a", "b"], 0}, {["c", "d"], 1}]),
  ?assertEqual({["a", "b"], 0}, ShortestPath).

secondTupleShouldBeShortest_test() ->
  ShortestPath = getShortestPath([{["c", "d"], 5}, {["a", "b"], 4}]),
  ?assertEqual({["a", "b"], 4}, ShortestPath).