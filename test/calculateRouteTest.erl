%%%-------------------------------------------------------------------
%%% @author skok
%%% @copyright (C) 2014, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 22. okt 2014 09:45
%%%-------------------------------------------------------------------
-module(calculateRouteTest).
-author("skok").

-include_lib("eunit/include/eunit.hrl").
-import(routeCalculator, [calculateRoute/3]).
-import(testRouteBuilder, [square_createNodes/0, square_createPaths/1, ode_createNodes/0, ode_createPaths/1, invalid_createNodes/0, invalid_createPaths/1]).

calculateSquareRouteAtoD_test() ->
  Nodes = square_createNodes(),
  Paths = square_createPaths(Nodes),
  StartNode = lists:nth(1, Nodes),
  EndNode = lists:nth(4, Nodes),
  CalculateRoute = calculateRoute(StartNode, EndNode, Paths),
  ?assertEqual({["a", "b", "c", "d"], 3}, CalculateRoute).

calculateODERoute0To13_test() ->
  Nodes = ode_createNodes(),
  Paths = ode_createPaths(Nodes),
  StartNode = lists:nth(1, Nodes),
  EndNode = lists:nth(14, Nodes),
  CalculateRoute = calculateRoute(StartNode, EndNode, Paths),
  ?assertEqual({["0", "1", "7", "8", "11", "13"], 70}, CalculateRoute).

calculateODERoute1To12_test() ->
  Nodes = ode_createNodes(),
  Paths = ode_createPaths(Nodes),
  StartNode = lists:nth(2, Nodes),
  EndNode = lists:nth(13, Nodes),
  CalculateRoute = calculateRoute(StartNode, EndNode, Paths),
  ?assertEqual({["1", "7", "8", "11", "12"], 48}, CalculateRoute).

invalidRoute0To13_test() ->
  Nodes = invalid_createNodes(),
  Paths = invalid_createPaths(Nodes),
  StartNode = lists:nth(1, Nodes),
  EndNode = lists:nth(14, Nodes),
  ?assertError(noRoutePossible, calculateRoute(StartNode, EndNode, Paths)).