%%%-------------------------------------------------------------------
%%% @author skok
%%% @copyright (C) 2014, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 23. okt 2014 14:54
%%%-------------------------------------------------------------------
-module(shortestPathTest).
-author("skok").

-include_lib("eunit/include/eunit.hrl").
-import(pathOperations, [updateShortestPath/5, updateShortestPathForAllNeighbours/5, addShortestPath/3, removeShortestPath/1]).

nodeShouldBeUpdatedWithNewDistance_test() ->
  Paths = updateShortestPath({"a", 0}, [], 0, [{["a"], 1}, {["b"], 2}], []),
  ?assertEqual([{["b"], 2}, {["a"], 0}], Paths).

nodeShouldBeUpdatedWithNewDistanceAndPath_test() ->
  Paths = updateShortestPath({"d", 1}, ["a", "b", "c"], 2, [{["a", "d"], 4}], []),
  ?assertEqual([{["a", "b", "c", "d"], 3}], Paths).

nodeWithHigherDistanceShouldNotBeUpdated_test() ->
  Paths = updateShortestPath({"d", 3}, ["a", "b", "c"], 2, [{["a", "d"], 4}], []),
  ?assertEqual([{["a", "d"], 4}], Paths).

nodeCShouldBeUpdated_test() ->
  Paths = updateShortestPath({"c", 1}, ["a", "b"], 1, [{["a", "d"], 4}, {["a", "e", "c"], 4}], []),
  ?assertEqual([{["a", "b", "c"], 2}, {["a", "d"], 4}], Paths).

nodeDShouldBeUpdated_test() ->
  Paths = updateShortestPath({"d", 1}, ["a", "b", "c"], 2, [{["a", "f"], 4}, {["a", "d"], 4}], []),
  ?assertEqual([{["a", "b", "c", "d"], 3}, {["a", "f"], 4}], Paths).

nodeBShouldBeUpdated_test() ->
  Paths = updateShortestPath({"b", 2}, ["a"], 1, [{["c", "b"], 4}, {["d", "b", "e"], 3}], []),
  ?assertEqual([{["d", "b", "e"], 3}, {["a", "b"], 3}], Paths).

newPathWithEqualLengthShouldBeUpdated_test() ->
  Paths = updateShortestPath({"c", 2}, ["a"], 0, [{["a","b","c"], 2}], []),
  ?assertEqual([{["a","c"], 2}], Paths).



multipleNodesShouldBeUpdatedWithNewDistance_test() ->
  Paths = updateShortestPathForAllNeighbours([{"c", 1}, {"d", 1}], ["a"], 1, [{["a", "c"], 3}, {["b", "d"], 3}], []),
  ?assertEqual([{["a", "c"], 2}, {["a", "d"], 2}], Paths).

twoOfThreeNodesShouldBeUpdatedWithNewDistance_test() ->
  Paths = updateShortestPathForAllNeighbours([{"c", 2}, {"e", 3}, {"d", 2}], ["a"], 1, [{["a", "b", "e"], 2}, {["a", "b", "c"], 4}, {["b", "d"], 3}], []),
  ?assertEqual([{["a", "d"], 3}, {["a", "c"], 3}, {["a", "b", "e"], 2}], Paths).



shouldAddFoundNeighboursToShortestPathList_test() ->
  ShortestPath = addShortestPath({["a"], 0}, [{"b", 1}, {"d", 4}], [{["a"], 0}]),
  ?assertEqual([{["a"], 0}, {["a", "b"], 1}, {["a", "d"], 4}], ShortestPath).

previousNodesShouldBeKept_test() ->
  ShortestPath = addShortestPath({["a", "b"], 1}, [{"c", 1}], [{["a"], 0}]),
  ?assertEqual([{["a"], 0}, {["a", "b", "c"], 2}], ShortestPath).

