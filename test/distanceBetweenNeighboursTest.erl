%%%-------------------------------------------------------------------
%%% @author skok
%%% @copyright (C) 2014, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 22. okt 2014 09:35
%%%-------------------------------------------------------------------
-module(distanceBetweenNeighboursTest).
-author("skok").

-include_lib("eunit/include/eunit.hrl").
-import(pathOperations, [getNeighbours/2, getDistanceBetweenNeighbours/3]).
-import(testRouteBuilder, [square_createPaths/1, square_createNodes/0]).

pathLengthBetweenAandBisOne_test() ->
  Nodes = square_createNodes(),
  Paths = square_createPaths(Nodes),
  Distance = getDistanceBetweenNeighbours(lists:nth(1, Nodes), lists:nth(2, Nodes), Paths),
  ?assertEqual(1, Distance).

pathLengthBetweenBandAisOne_test() ->
  Nodes = square_createNodes(),
  Paths = square_createPaths(Nodes),
  Distance = getDistanceBetweenNeighbours(lists:nth(2, Nodes), lists:nth(1, Nodes), Paths),
  ?assertEqual(1, Distance).

pathLengthBetweenAandDisFour_test() ->
  Nodes = square_createNodes(),
  Paths = square_createPaths(Nodes),
  Distance = getDistanceBetweenNeighbours(lists:nth(1, Nodes), lists:nth(4, Nodes), Paths),
  ?assertEqual(4, Distance).

pathLengthBetweenDandAisFour_test() ->
  Nodes = square_createNodes(),
  Paths = square_createPaths(Nodes),
  Distance = getDistanceBetweenNeighbours(lists:nth(4, Nodes), lists:nth(1, Nodes), Paths),
  ?assertEqual(4, Distance).

pathLengthBetweenBandCisOne_test() ->
  Nodes = square_createNodes(),
  Paths = square_createPaths(Nodes),
  Distance = getDistanceBetweenNeighbours(lists:nth(2, Nodes), lists:nth(3, Nodes), Paths),
  ?assertEqual(1, Distance).

pathLengthBetweenCandBisOne_test() ->
  Nodes = square_createNodes(),
  Paths = square_createPaths(Nodes),
  Distance = getDistanceBetweenNeighbours(lists:nth(3, Nodes), lists:nth(2, Nodes), Paths),
  ?assertEqual(1, Distance).

pathLengthBetweenCandDisOne_test() ->
  Nodes = square_createNodes(),
  Paths = square_createPaths(Nodes),
  Distance = getDistanceBetweenNeighbours(lists:nth(3, Nodes), lists:nth(4, Nodes), Paths),
  ?assertEqual(1, Distance).

pathLengthBetweenDandCisOne_test() ->
  Nodes = square_createNodes(),
  Paths = square_createPaths(Nodes),
  Distance = getDistanceBetweenNeighbours(lists:nth(4, Nodes), lists:nth(3, Nodes), Paths),
  ?assertEqual(1, Distance).

getPathLengthOfNonNeighboursShouldReturnError_test() ->
  Nodes = square_createNodes(),
  Paths = [{"a", "b", 1}, {"a", "c", 1}, {"a", "d", 1}],
  ?assertError(badArgument, getDistanceBetweenNeighbours(lists:nth(3, Nodes), lists:nth(4, Nodes), Paths)).

getPathLengthOfEmptyPathsShouldReturnError_test() ->
  Nodes = square_createNodes(),
  Paths = [],
  ?assertError(badArgument, getDistanceBetweenNeighbours(lists:nth(3, Nodes), lists:nth(4, Nodes), Paths)).
